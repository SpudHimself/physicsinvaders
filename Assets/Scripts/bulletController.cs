﻿using UnityEngine;
using System.Collections;

public class bulletController : MonoBehaviour
{
    //components
    private Rigidbody2D m_rb;
    private Collider2D m_collide;
    public playerController m_player;
    public GameObject m_deathParticle;

    //variables
    public float m_moveSpeed;

    // Use this for initialization
    void Start()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_collide = GetComponent<Collider2D>();
        m_player = FindObjectOfType<playerController>();
    }

    // Update is called once per frame
    void Update()
    {
        m_rb.velocity = new Vector2(m_rb.velocity.x, m_moveSpeed);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        //hit enemy, kill enemy
        if (other.gameObject.tag == "Enemy")
        {
            //points!
            scoreManager.score += 100;
            //also ded
            Destroy(other.gameObject);
        }

        //destroy self and decrease fire amount
        m_player.fireAmount--;
        Destroy(gameObject);
    }
}
