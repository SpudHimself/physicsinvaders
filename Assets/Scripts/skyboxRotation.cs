﻿using UnityEngine;
using System.Collections;

public class skyboxRotation : MonoBehaviour
{

    private Skybox m_skybox;

    public float rotationSpeed;

    // Use this for initialization
    void Start()
    {
        m_skybox = GetComponent<Skybox>();
    }

    // Update is called once per frame
    void Update()
    {
        m_skybox.transform.Rotate( 0.0f, rotationSpeed, 0.0f);
    }
}
