﻿using UnityEngine;
using System.Collections;

public class playerController : MonoBehaviour
{
    //player components (lazy way beucase lol update)
    private Rigidbody2D m_rb;
    private Collider2D m_collide;

    //player variables
    public float m_moveSpeed;
    public float m_direction;

    //for shooting
    public Transform firePoint;
    public GameObject bullet;
    public int fireLimit;
    public int fireAmount;

    // Use this for initialization
    void Start()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_collide = GetComponent<Collider2D>();
        //Screen.SetResolution(720, 1280, false); //careful now, find out the res that suits the projector

    }

    // Update is called once per frame
    void Update()
    {
        //update the movement float
        m_direction = Input.GetAxis("Horizontal");

        //right
        if (m_direction > 0)
            m_rb.velocity = new Vector2(m_moveSpeed * m_direction, 0.0f);
        //left
        else if (m_direction < 0)
            m_rb.velocity = new Vector2(m_moveSpeed * m_direction, 0.0f);
        //deadzone issue on my controller? not sure
        else
            m_rb.velocity = new Vector2(0.0f, 0.0f);

        if (Input.GetButtonDown("Jump") && fireAmount < fireLimit)
        {
            Instantiate(bullet, firePoint.position, firePoint.rotation);
            fireAmount++;
        }

    }
}
