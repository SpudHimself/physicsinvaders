﻿using UnityEngine;
using System.Collections;

public class levelManager : MonoBehaviour
{
    public int amountOfEnemies;
    bool killedAllEnemies = false;
    public string nextLevel;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] enemiesArray = GameObject.FindGameObjectsWithTag("Enemy");
        amountOfEnemies = enemiesArray.Length;

        if (amountOfEnemies == 0)
            killedAllEnemies = true;

        if (killedAllEnemies)
            loadNextLevel(nextLevel);

    }

    void loadNextLevel(string levelName)
    {
        Application.LoadLevel(levelName);
        scoreManager.created = false;
    }
}
