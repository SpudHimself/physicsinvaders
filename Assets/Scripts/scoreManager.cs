﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class scoreManager : MonoBehaviour
{
    public static int score = 000000;        // The player's score.
    public static int highScore = 000000;   //the high score

    Text text;                      // Reference to the Text component.

    public static bool created = false;

    void Awake()
    {
        // Set up the reference.
        text = GetComponent<Text>();


        // Reset the score.

        if (text.tag == "Score")
        {
            if (!created)
            {
                DontDestroyOnLoad(this.gameObject);
                created = true;
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

    }


    void Update()
    {
        //score handling
        if (text.tag == "Score")
        {
            if (score > 000000 && score <= 999990)
                text.text = score.ToString("D6"); //six 0 padding
            else if (score <= 000000)
                text.text = "000000";
            else
                text.text = "999990";

            if (score > highScore)
            {
                highScore = score;
            }
        }



        if (text.tag == "HighScore")
            text.text = highScore.ToString("D6");
    }

}