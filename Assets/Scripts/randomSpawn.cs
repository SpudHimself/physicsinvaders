﻿using UnityEngine;
using System.Collections;

public class randomSpawn : MonoBehaviour {
	
	public GameObject enemy;                // The block prefab to be spawned.
	public float spawnTime = 3.0f;          // How long between each spawn.
	Vector2 spawnVec;
	Quaternion spawnRot;

	public Transform boundaryLeft;
	public Transform boundryRight;
	public Transform boundryHeight;
	
	// Use this for initialization
	void Start () 
	{
		InvokeRepeating ("Spawn", spawnTime, spawnTime);
	}
	
	void Spawn ()
	{
		
		// Find a random index
		float spawnX = Random.Range (boundaryLeft.transform.position.x, boundryRight.transform.position.x);

		
		spawnVec.Set(spawnX, boundryHeight.transform.position.y);
		spawnRot.Set (0, 0, 0, 0);
		
		// Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
		Instantiate (enemy, spawnVec, spawnRot);
	}
}


//x = -15 to 15
//z =  -5 to 25
//to stop blocks falling on platforms that aint there, could just have spawnpoints above each platform and spawn it in a random range from that spawn point
//when the platform falls off, could set that spawnpoint to null so it doesnt spawn blocks there anymore