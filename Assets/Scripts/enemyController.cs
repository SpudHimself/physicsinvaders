﻿using UnityEngine;
using System.Collections;

public class enemyController : MonoBehaviour
{
    //components
    private Rigidbody2D m_rb;
    private Collider2D m_collide;
    public playerController m_player;

    //variables
    public float m_torque;

    // Use this for initialization
    void Start()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_collide = GetComponent<Collider2D>();
        m_player = FindObjectOfType<playerController>(); 
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        //hit block, destroy block
        if (other.gameObject.tag == "Block")
        {
            scoreManager.score -= 10;
            Destroy(other.gameObject);
            m_rb.AddTorque(m_torque);
        }

        //hit player, kill player
        if (other.gameObject.tag == "Player")
        {
            Destroy(other.gameObject);
            Application.LoadLevel("titleScreen");
        }
    }
}
