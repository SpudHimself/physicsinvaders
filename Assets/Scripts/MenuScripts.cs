﻿using UnityEngine;
using System.Collections;

public class MenuScripts : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void LoadScene(string levelName)
    {
        Application.LoadLevel(levelName);
        if (scoreManager.created == true)
        {
            scoreManager.created = false;
            scoreManager.score = 000000;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
